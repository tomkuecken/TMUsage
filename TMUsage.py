#!/usr/bin/python3

import sqlite3
import os
import time
from datetime import datetime as dt


SCHEMA_SCRIPT = 'create_schema.sql'
DB_NAME = 'tm.db'
STAGING_TABLE = 'Stag_UsageVoice'

class Table(object):
    def __init__(self, table_name: str, columns: dict, rows: []=None):
        self.table_name = table_name
        self.columns  = columns
        self.rows = rows

class VoiceStaging(Table):
    def __init__(self, conn: sqlite3.Connection):
        self.conn = conn
        table_name = 'Stag_VoiceUsage'
        columns = {
            'Number': 'text',
            'Date': 'text',
            'Destination': 'text',
            'Number': 'text',
            'Minutes': 'integer',
            'Call Type': 'text',
            None: 'text' # used for money amount if overage
        }
        super().__init__(self, table_name, columns)

    def get_rows(self):
        cur = self.conn.cursor()
        try:
            cur.execute(""" SELECT
                                field1,
                                field2,
                                field3,
                                field3,
                                field5,
                                field6,
                                field7,
                                field8
                            FROM Stag_VoiceUsage""")
            rows = cur.fetchall()
            if not rows:
                return


            for row in rows:
                try:
                    self.rows.append({
                        'Number': row[0],
                        'Date': row[1],
                        ''
                    })

class Database(object):
    def __init__(self)
        self.db_name = DB_NAME
        self.conn = sqlite3.connect(self.db_name)
        self.create_schema()

    def create_schema(self):
        if not os.path.exists(SCHEMA_SCRIPT):
            raise IOError

        cur = self.conn.cursor()
        try:
            cur.executescript(SCHEMA_SCRIPT)
            self.conn.commit()
        except sqlite3.Error as e:
            self.conn.rollback()
            print(e)
        finally:
            cur.close()

    def clean_voice_staging(self)

def main():
    db = Database()

if __name__ == '__main__':
    main()